/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.sql.types

import org.json4s._
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._


case object GeometryType extends GeometryType

class GeometryType extends DataType {

  private[sql]type JvmType = Array[Byte]
  /** The default size of a value of this data type. */
  override def defaultSize: Int = 0

  override def typeName: String = "GeometryType"
  /**
   * Check if `this` and `other` are the same data type when ignoring nullability
   */
  override private[spark] def sameType(other: DataType): Boolean =
    this.typeName.equals(other.typeName)
  override private[sql] def jsonValue: JValue = {
    ("class" -> JString("GeometryType")) ~
      ("sqlType" -> JString("ArrayType(ByteType)"))
  }
  /**
   * Returns the same data type but set all nullability fields are true
   */
  override private[spark] def asNullable: DataType = this
}